# Overview

For development and deployment of GitLab CE's Continuous Integration & Continuous Deployment (CI/CD) Amazon EKS we must estimate the upper and lower limits of the estimated cost for budget considerations. In this document we will outline the following items:

* Break down of Amazon EKS costs
* Our methodology for estimating cost of a GitLab CI/CD instances using Amazon EKS Clusters
* Examples of potential real world uses-cases of GitLab CE's CI/CD
* Templates used for EKS cost estimation
* IEEE SA Open's results from our cost estimate benchmarks

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Overview](#overview)
- [Amazon EKS Costs](#amazon-eks-costs)
- [Methodology](#methodology)
- [Template](#template)
- [Benchmark Results](#benchmark-results)
- [CI/CD Use Cases](#cicd-use-cases)
- [Open Questions](#open-questions)
- [References](#references)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
# Amazon EKS Costs

To calculate costs we make the following assumptions:

* We are using _a1.medium_ EC2 instances
* We will be transferring less than 50 TB per month to the S3s
* We will be transferring less than 9.999 TB per month

Amazon EKS charges can be broken up into the follow categories:

| Service     | Cost        | Notes |
| ----------- | ----------- | ---
| AWS EKS Cluster | $0.10/hour  | Always billed |
| AWS EC2 Instances | $0.0255/hour | Billed only during use |
| AWS EC2 Data Transfer | $0.09/GB | Up to 9.999 TB/month |
| AWS S3 | $0.023/GB | Up to 50 TB/month |

# Methodology

In order to properly estimate the potential cost ceiling and floors for this project we will be using test-cases based on real-world use cases. While these tests will not fully simulate the actions of experienced power-users they will provide an estimate of what the average project will cost while still providing reasonable execution times for the end-user. When testing the following data must be captured for each test case:

* Bandwidth Used (from EC2 instance)
* Average Execution time
* Amount of S3 Storage Required

These metrics can be used to extrapolate maximum users each worker can realistically support, which will allow us to calculate the cluster's cost floor an ceiling.

# Template

| Use Case | Avg. Execution Time | EC2 Bandwidth | S3 Storage |
| -------- | ------------------- | ------------ | ---------- |
|          |                     |              |            |

# Benchmark Results

TBA

# CI/CD Use Cases

The following is a table of real world applications of GitLab's CI/CD. These use cases are used to generate tests to capture the costs (data transfer, storage, and EC2). Additionally we have come up with a comparable test case we can use to roughly simulate these actions.

| Use Case | Description | Test Case |
| -------- | ----------- | --------- |
| GitLab Pages | Allows users to generate static content websites | Test GitLab Pages with gitbook (NodeJS) |
| Lint Checking | Scans the user's source code for syntax errors and/or compliance with coding standards | Run GitLab CE's Lint Checker |
| Software Specific Test Cases | Run GitLab CE's Test Suite |

# Open Questions

* What size EC2 instance will the worker nodes be? (medium?)
* What are reasonable wait times for the average CI/CD job to complete? (5 mins)

# References

* (EKS Pricing)[https://aws.amazon.com/eks/pricing/]
* (EC2 Pricing)[https://aws.amazon.com/ec2/pricing/on-demand/]

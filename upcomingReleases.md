# Upcoming Releases

## 2.0.0
	
### Offerings

* Continuous Integration & Continuous Deployment (CI/CD)
  * Documentation for for use of CI/CD on opensource.ieee.org
  * Access to CI/CD pipelines to their repositories
	* Access to Auto-CI
  * Access to our registry of pre-built images 
  * Ability to provide your own pre-built images to the registry
  * Paid access to private EKS Clusters
  * Technical Support
	* Access to community based support via MatterMost
	* Access to IEEE SA Open Help-desk support via email/GitLab tickets

* GitLab Pages
  * Access to host static websites
  * Pre-defined templates provided to make deployment of your static website quick

* Enhanced Architecture
  * Dedicated MatterMost server for Community Chat
  * Dedicated tools server for hosting productivity enhancing applications such as PlantUML for creating beautiful diagrams
  * Dedicated gitaly server for load balanced access to GitLab Repositories
	* Additional gitaly servers can be added as the project scales

* Technical Support
  * Access to community based support via MatterMost
  * Access to IEEE SA Open Help-desk support via email/GitLab tickets

* Staff Training
  * Periodic Webinar's on the following topics:
    * General training's on how to maximize your teams productivity on GitLab
	* How to get the most use out of CI/CD for your team
	* How to setup and maintain GitLab Page

* Peer Review

## 2.1.0

### Offerings

* Continuous Integration & Continuous Deployment (CI/CD)
 * Multiple architecture support to allow projects to test against various CPU architectures

* Review Applications
  * Paid access to review apps to host your project

* BigBlueButton for MatterMost

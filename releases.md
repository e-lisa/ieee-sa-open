# Releases

IEEE SA Open releases are based off [_Semantic Versioning v2.0.0_](https://semver.org/), with the additional caveat of working downstream from GitLab CE. Specifically our releases are defined as _MAJOR.MINOR.PATCH_, the version numbers increment when the following conditions are met:

* **MAJOR**: Large component has been added by IEEE SA Open or GitLab CE that is incompatible with previous releases
* **MINOR**: New component has been added by IEEE SA Open or GitLab CE that are compatible with the previous releases
* **PATCH**: Bug or security fix has been made to an existing components that is fully compatible with previous releases

